const btnFind = document.querySelector('.btn')
const ipDataDiv = document.querySelector('.ip')

const urlIp = 'https://api.ipify.org/?format=json';
const urlIpInfo = 'http://ip-api.com/json/';

async function sendRequest(url){
    let response = await fetch(url);
    if (response.status == 200) {
        let json = await response.json();
        
        return json; 
        
      }
    
      throw new Error(response.status);
    }
   

    class Data {
                constructor({countryCode, country, region, city, regionName,query,timezone, isp, org}){
                    this.countryCode = countryCode;
                    this.country = country;
                    this.region = region;
                    this.city = city;
                    this.regionName = regionName;
                    this.query = query;
                    this.timezone = timezone;
                    this.isp = isp;
                    this.org = org;
                }
        
                renderInfo(){
                    this.info = document.createElement('div');
                    this.info.classList.add('card');
                    this.info.insertAdjacentHTML('afterbegin', `
                    
                    <h2 class='card__title'>Ваш IP:</h2>
                        <p  class='card__field'>${this.query}</p>
                    <h2 class='card__title'>Код країни:</h2>
                        <p  class='card__field'>${this.countryCode}</p>
                    <h2 class='card__title'>Країна:</h2>
                        <p  class='card__field'>${this.country}</p>
                    <h2 class='card__title'>Область:</h2>
                        <p  class='card__field'>${this.region}</p>
                    <h2 class='card__title'>Місто:</h2>
                        <p  class='card__field'>${this.city}</p>
                    <h2 class='card__title'>Назва регіону:</h2>
                        <p  class='card__field'>${this.regionName}</p>
                    <h2 class='card__title'>Часовий пояс:</h2>
                        <p  class='card__field'>${this.timezone}</p>
                    <h2 class='card__title'>Провайдер:</h2>
                        <p  class='card__field'>${this.isp}</p>
                    <h2 class='card__title'>Організація:</h2>
                        <p  class='card__field'>${this.org}</p>
                    
                    `)
                    ipDataDiv.insertAdjacentElement('afterend', this.info)
                } 
            }

                btnFind.addEventListener('click', async()=>{
                    const ipInfo = await sendRequest(urlIp)
                    const data = await sendRequest(`http://ip-api.com/json/${ipInfo.ip}?fields=status,message,country,countryCode,region,regionName,city,zip,lat,lon,timezone,isp,org,as,query`);
                        new Data(data).renderInfo()
                 })
